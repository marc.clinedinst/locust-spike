from locust import between, HttpUser, task


class QuickstartUser(HttpUser):
    wait_time = between(1, 2.5)

    @task
    def get_movies(self):
        self.client.get("/api/movies/")
